<h1> Cenário: Transportadora Entrega Rápida </h1>

<p>
A Transportadora Entrega Rápida está com um plano de expansão e entende que para
ter sucesso em suas atividades se faz necessário o desenvolvimento de um Sistema de
Logística.
A transportadora trabalha apenas com objetos grandes e todas as suas encomendassão
colocadas em caixas iguais, dessa forma fica mais fácil organizar as caixas dentro dos
veículos.
Quem irá acessar o sistema são os funcionários da frente de caixa da transportadora,

que devem fazer todos os cadastros necessários, ao cadastrar o objeto, deve-se encaixá-
lo em um dos veículos disponíveis em um dos dias da semana (segunda a sábado).

Todas as manhãs, o atendente solicita ao sistema a geração dos roteiros de entrega para
todo o dia (ver RF004). Para a distribuição das cargas por veiculo, deve-se
primeiramente optar pelo veiculo de maior capacidade para o de menor capacidade,
mais detalhes a seguir:
<ul>
<li> Carreta: 10 pacotes; </li>
<li> Caminhão baú: 3 pacotes; </li>
<li> Van: 1 pacote.</li>
</ul>
Pode-se considerar que o veículo só vai ser utilizado uma vez por dia e consegue ir em
todos os pontos de entrega referentes aos objetos de sua capacidade. Os objetos devem
ser distribuídos pelos veículos em ordem crescente à data de cadastro.
Todo motorista deve no início do dia receber o seu roteiro de entrega, ele deve anotar
no papel recebido, quais os objetos que foram entregues, bem como os que não foram
entregues, deixando disponível para o funcionário da frente de caixa essa informação
de modo que ele possa informar no sistema.
</p>
<p>
<h1>Requisitos do sistema </h1>
<ol>
<li>
  <strong>
    O sistema deve permitir o cadastro dos veículos.
  </strong>
 Descrição: os veículos podem ser dos seguintes tipos: van, caminhão baú ou carreta.
Deve-se armazenar os seguintes dados do veículo: marca, modelo, ano e placa.
</li>
  <li>
  <strong>
    O sistema deve armazenar as informações do motorista.
  </strong>
Descrição: deve-se armazenar as seguintes informações: nome, data de nascimento,
endereço, tipo da CNH e número da CNH.
Regra de Negócio: 1.
</li>
<li>
  <strong>
    O sistema deve armazenar as informações relacionadas ao objeto para entrega.
  </strong>
Descrição: deve-se armazenar as seguintes informações do objeto, nome do remetente,
endereço do remetente, nome do destinatário, endereço do destinatário, data do
deposito do objeto, peso e código localizador.
Regra de Negócio: 2.
</li>  
  <li>
  <strong>
    O sistema deve permitir a geração dos roteiros diários por veículo.
  </strong>
Descrição: O sistema deve a partir da frota cadastrada distribuir todas as encomendas,
ao final do processo, deve-se apresentar na tela a relação dos objetos alocados por
veículo. Deve-se dar a opção para o usuário verificar os objetos que ficaram fora do
roteiro do dia (ver: 5).
Regra de Negócio: 3; 4.
</li>
<li>
  <strong>
    O sistema deve permitir os objetos que ficaram fora do roteiro do dia possam ser listados.
  </strong>
Descrição: Não há garantia que os objetos que foram selecionados para ser entregues
no dia sejam realmente entregues, por conta disso se faz necessário visualizar as
entregas que não foram entregues no dia presente ou num dia anterior.
</li>
 <li>
  <strong>
    O sistema deve permitir que os funcionários informem no roteiro do dia atual quais objetos não foram entregues.
  </strong>
Descrição: O funcionário da frente de caixa recebe no final do dia de cada motorista um
papel com a informação, da entrega ou não, do objeto que estava na rota do motorista
em questão.
</li>
 <li>
  <strong>
    O sistema deve permitir que se vincule um motorista com um veículo.
  </strong>
Descrição: Antes de fazer uma rota para um motorista, deve-se vincular um veículo para
ele, a partir dessa vinculação é conhecido a quantidade de objetos que ele deve entregar
por dia. Pode-se alterar a vinculação do motorista por veículo.
Regra de Negócio: 4
</li>
 <li>
  <strong>
    O sistema deve permitir que sejam listados roteiros antigos a partir da sua data.
  </strong>
Descrição: Pode-se listar os roteiros antigos por data e/ou por motorista.
</li>
</ol>
<p>
<h1>Regras de negócio </h1>
<ol>
  <li>
  <strong>
    Tipo de CNH por tipo de Veículo
  </strong>
Descrição:
<ul>
<li>- Somente motoristas com classe B e/ou C de CNH, podem dirigir veículos do tipo Van;</li>
<li>- Somente motoristas com classe C de CNH podem dirigir o veículo do tipo caminhão baú ou carreta.</li>

</ul>
</li>
<li>
  <strong>
    Código localizador do objeto
  </strong>
Descrição:
<ul>
<li> - o código localizador do objeto deve ser único e pode ser cadastrado pelo usuário;</li>
<li> caso o usuário não digite o código localizador no cadastro, deve-se gerar um
automaticamente.</li>
</ul>
</li>  
  <li>
  <strong>
    Distribuição de objetos por veículo
  </strong>
Descrição:
<ul>
<li>  Os objetos devem ser distribuídos em ordem crescente a partir da sua data ou id de
inserção;</li>
<li> Deve-se primeiro preencher os veículos com maiores capacidades para os menores,
caso tenham objetos que não foram alocados no roteiro do dia, o mesmo ficará
disponível para o próximo roteiro.</li>
</ul>
</li>
<li>
  <strong>
    Vinculo de motoristas por veículo
  </strong>
Descrição:
<ul>
<li>  um veículo só pode estar vinculado à um motorista;</li>
<li> só é permitir gerar roteiro de entrega para os motoristas com veículos.</li>
</ul>
</li>

</ol> 

</p>
