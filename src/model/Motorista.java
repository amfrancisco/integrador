/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entities;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Junior
 */
public class Motorista extends Pessoa{
    
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            
    private Date dataNascimento;
    private String tipoCNH;
    private String numeroCNH;

    public Motorista() {
    }

    public Motorista(Date dataNascimento, String tipoCNH, String numeroCNH, int id, String nome, Endereco endereco) {
        super(id, nome, endereco);
        this.dataNascimento = dataNascimento;
        this.tipoCNH = tipoCNH;
        this.numeroCNH = numeroCNH;
    }

   

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getTipoCNH() {
        return tipoCNH;
    }

    public void setTipoCNH(String tipoCNH) {
        this.tipoCNH = tipoCNH;
    }

    public String getNumeroCNH() {
        return numeroCNH;
    }

    public void setNumeroCNH(String numeroCNH) {
        this.numeroCNH = numeroCNH;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append("MOTORISTA");
        sb.append("ID: " + super.getId());
        sb.append(super.toString());
        sb.append("DOCUMENTO MOTORISTA");
        sb.append("Tipo CNH: " + tipoCNH);
        sb.append("Numero CNH: " +numeroCNH);
        
        return sb.toString();
    }
    
    
}
