/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entities;

/**
 *
 * @author Junior
 */
public class Cliente extends Pessoa{
    
    public Cliente() {
    }

    public Cliente(int id, String nome, Endereco endereco) {
        super(id, nome, endereco);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append("CLIENTE - ");
        sb.append("ID: " + super.getId());
        sb.append(super.toString());
        
        return sb.toString();
    } 

}
