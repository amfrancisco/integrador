
package model.entities;

import java.util.Date;

public class Motorista extends Pessoa{
    
   
    private Date dataNascimento;
    private String tipoCNH;
    private String numeroCNH;

    public Motorista() {
    }

    public Motorista(Date dataNascimento, String tipoCNH, String numeroCNH, int id, String nome, String rua, String bairro, String cidade, String estado, String pais, int numero) {
        super(id, nome, rua, bairro, cidade, estado, pais, numero);
        this.dataNascimento = dataNascimento;
        this.tipoCNH = tipoCNH;
        this.numeroCNH = numeroCNH;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getTipoCNH() {
        return tipoCNH;
    }

    public void setTipoCNH(String tipoCNH) {
        this.tipoCNH = tipoCNH;
    }

    public String getNumeroCNH() {
        return numeroCNH;
    }

    public void setNumeroCNH(String numeroCNH) {
        this.numeroCNH = numeroCNH;
    }
       
}
