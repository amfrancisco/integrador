/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.util.ArrayList;
import model.entities.Produto;

/**
 *
 * @author antonio
 */
public class CadastroProduto {
    
    private static ArrayList<Produto> listaProdutos = new ArrayList<>();
    static boolean getListaP;

    public static ArrayList<Produto> getListaProdutos() {
        return listaProdutos;
    }
    
    public static void adicionar(Produto p){
        listaProdutos.add(p);
    }
    
    public static String listar(){
        String saida = "";
        int i = 1;
        for(Produto p : listaProdutos){
            saida += "\n======= PRODUTO Nº " + (i++) + " =======\n";
            saida += p.imprimir() + "\n";
        }
            
        return saida;
    }
    
    public static boolean remover(Integer idLoc){
        for(Produto p : listaProdutos){
            if(p.getIdLoc().equals(idLoc)){
                listaProdutos.remove(p);
                return true;
            }
        }
        return false;
    }

    static Object getListaProduto() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
