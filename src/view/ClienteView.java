/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javax.swing.JOptionPane;
import model.entities.Cliente;
import model.entities.Endereco;

/**
 *
 * @author Junior
 */
public class ClienteView {
    
     public char menuCliente(){
        
        String menu = "MENU CLIENTE \n" + 
                      "1 - Cadastrar \n" +
                      "2 - Remover \n" +
                      "3 - Consultar \n" +
                      "4 - Listar \n" +
                      "0 - Retornar ao Menu Principal \n";
        
        return JOptionPane.showInputDialog(menu).charAt(0);
    } 
     
    public Cliente cadastrar(){
        Cliente cleinte = new Cliente();
        Endereco endereco = new Endereco();
        
        cleinte.setNome(JOptionPane.showInputDialog("Nome: "));
        endereco.setRua(JOptionPane.showInputDialog("Rua: "));
        endereco.setCidade(JOptionPane.showInputDialog("Cidade: "));
        endereco.setBairro(JOptionPane.showInputDialog("Bairro: "));
        endereco.setEstado(JOptionPane.showInputDialog("Estado: "));
        endereco.setPais(JOptionPane.showInputDialog("Pais: "));
        int num = Integer.parseInt(JOptionPane.showInputDialog("Numero: "));
        endereco.setNumero(num);
        
        cleinte.setEndereco(endereco);
        
        
        return cleinte;
    }
    
    public String remover(){
        return consultar();
    }
    
    public String consultar(){
        return JOptionPane.showInputDialog("Nome: ");
    }
    
    public void listar(Cliente cliente){
        JOptionPane.showMessageDialog(null, cliente);
    }

}
